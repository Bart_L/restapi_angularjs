'use strict';

var controllersNavigation = angular.module( 'controllersNavigation' , [] );


controllersNavigation.controller( 'navigation' , [ '$scope' , '$location' , 'cartSrv' , 'checkToken' , 'store' , function( $scope , $location , cartSrv , checkToken , store ){
    //location.path() zwraca obecną lokalizację (adres)
    // console.log($location.path());


	$scope.navigation = function () {
        //.test testuje czy poprzedzające wyrażenie regularne występuje w adresie podanym jako argument w nawiasie
		if ( /^\/admin/.test( $location.path() ) )
		{
            //jeżeli użytkownik nie jest Adminem, wtedy nie wyswietlaj uzytkowników
			if ( !checkToken.isAdmin() )
			{	
				window.location.href = '#/products?alert=noAdmin';
			}

			return 'partials/admin/navigation.html';

		}
		else
		{
			if ( $location.search().alert == 'noAdmin' )
				$scope.noAdmin = true;
			else
				$scope.noAdmin = false;


			if ( checkToken.loggedIn() )
				$scope.loggedIn = true;
			else
				$scope.loggedIn = false;


			if ( checkToken.isAdmin() )
				$scope.isAdmin = true;
			else
				$scope.isAdmin = false;

			return 'partials/site/navigation.html';
		}
	};


	$scope.isActive = function ( path ) {
		return $location.path() === path;
	};

	$scope.$watch(function(){
		$scope.cart = cartSrv.show().length;
	});

    //funckja wylogowująca, odnosi się do servisu w ktorym czysci local storage
	$scope.logout = function () {
		checkToken.del();
        //natywna funkcja js ,przeladowanie okna po zalogowaniu ,zeby skorzystac z zapisanych ciasteczek
        location.reload();
	};

	$scope.szablon = store.get( 'szablon' );
	$scope.$watch(function(){
		store.set( 'szablon' , $scope.szablon );		
	});

}]);


