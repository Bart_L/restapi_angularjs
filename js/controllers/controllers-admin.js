'use strict';

var controllersAdmin = angular.module( 'controllersAdmin' , [ 'angularFileUpload' , 'myDirectives' ] );


controllersAdmin.controller( 'products' , [ '$scope' , '$http' , 'checkToken' , function( $scope , $http , checkToken ){
    // wywołanie zapytania http do database
	$http.post( 'api/admin/products/get' , {
		token: checkToken.raw()
        //callback wywołujący się, kiedy połączymy się z bazą
    }).success( function( data ){
        // przypisywanie zmiennej scope do odpowiedzi z pliku
		$scope.products = data;
        //callback kiedy jest błąd
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

	$scope.delete = function ( product , $index ) {
        //jeżeli admin nie potwierdzi usuwania, to skrypt poniżej zostanie zatrzymany
		if ( !confirm( 'Czy na pewno chcesz usunąć ten produkt?' ) )
			return false;
        // spliceowanie tablicy param 1 - index usuwanego obiektu; param 2 - ile elementów od tego elementu usunąć; param 3 - jezeli w drugim podamy 0 to znaczy ze nie usuwamy i jako trzeci mozemy wstawic nowy obiekt wstawiany za obecny np. {nowy}
		$scope.products.splice( $index , 1 );

		$http.post( 'api/admin/products/delete/' , {
			token: checkToken.raw(),
			product : product
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

        console.log('usunięto obiekt: ', product, 'o indeksie: ', $index);

    };
}]);


controllersAdmin.controller( 'productEdit' , [ '$scope' , '$http' , '$routeParams' , 'FileUploader' , '$timeout' , 'checkToken' , function( $scope , $http , $routeParams , FileUploader , $timeout , checkToken ){

	var productId = $routeParams.id;
	$scope.id = productId;

    // wywołanie zapytania http do db

	$http.post( 'api/admin/products/get/' + productId , {
		token: checkToken.raw()
        //callback wywołujący się, kiedy połączymy się z bazą
    }).success( function( data ){
        // przypisywanie zmiennej scope do odpowiedzi z pliku , przypisuje wszystko co zostanie odesłane
        $scope.product = data;
        //callback kiedy jest błąd
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
	$scope.saveChanges = function ( product ) {

		$http.post( 'api/admin/products/update/' , {
			token: checkToken.raw(),
			product : product
		}).success( function(){
			$scope.success = true;
            //angularowy timeout uzyty tutaj w celu przypisania do powyzszego success wartosci false po 3 sekundach, co spowoduje zniknięcie alertu o sukcesie po trzech sekundach (zeby nie wisial niepotrzebnie na stronie )
			$timeout(function(){
				$scope.success = false;
			} , 3000 );
            //callback kiedy jest błąd
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};


	function getImages() {
        //zapytanie o zdjęcia obiektu o konkretnym id
		$http.post( 'api/admin/images/get/' + productId , {
			token: checkToken.raw()
		}).success( function( data ){
			$scope.images = data; 
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});
	}
	getImages();

    var uploader = $scope.uploader = new FileUploader({
        url : 'api/admin/images/upload/' + productId // sciezka do ścieżki wyświetlanej w adresie przegladarki (w funkcji upload  w phpowym kontrollerze images.php)
    });

    // FILTERS dla uploadera

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    //dodanie funkcji getImg kiedy akcja jest complete, sluzy do tego zeby zdjecia pokazywaly sie od razu po uploadzie a nie po odsiwzeniu
	uploader.onCompleteItem = function(fileItem, response, status, headers) {
        getImages();
    };

    $scope.delImage = function ( imageName , $index ) {
        //usuwamy z widoku obrazek za pomocą indexu przekazywanego w funkcji
		$scope.images.splice( $index , 1 );

		$http.post( 'api/admin/images/delete/' , {

			token: checkToken.raw(),
			id : productId,
			image : imageName
            //UWAGA!!! zamiast używania powyższego splice na images w celu odświeżenia widoku, możemy usunąć powyższy splice, a tutaj w successCallback przywolac funkcjję getImg();

		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

    };

    $scope.setThumb = function ( product , image ) {

		$http.post( 'api/admin/images/setThumb/' , {

			token: checkToken.raw(),
			product : product,
			image : image

		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

    };


}]);


controllersAdmin.controller( 'productCreate' , [ '$scope' , '$http' , '$timeout' , 'checkToken' , function( $scope , $http , $timeout , checkToken ){
    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
	$scope.createProduct = function ( product ) {

		$http.post( 'api/admin/products/create/' , {
			token: checkToken.raw(),
			product : product
		}).success( function(){
			$scope.success = true;
            //angularowy timeout uzyty tutaj w celu przypisania do powyzszego success wartosci false po 3 sekundach, co spowoduje zniknięcie alertu o sukcesie po trzech sekundach , a także wyczyszcenie inputów ze scope(zeby nie wisial niepotrzebnie na stronie )
			$timeout(function(){
				$scope.success = false;
				$scope.product = {};
			} , 3000 );

		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};

}]);


controllersAdmin.controller( 'users' , [ '$scope' , '$http' , 'checkToken' , function( $scope , $http , checkToken ){

	$http.post( 'api/admin/users/get' , {
		token: checkToken.raw()
	}).success( function( data ){
		$scope.users = data;
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

	$scope.delete = function ( user , $index ) {
        //jeżeli admin nie potwierdzi usuwania, to skrypt poniżej zostanie zatrzymany
		if ( !confirm( 'Czy na pewno chcesz usunąć tego użytkownika?' ) )
			return false;
        // spliceowanie tablicy param 1 - index usuwanego obiektu; param 2 - ile elementów od tego elementu usunąć; param 3 - jezeli w drugim podamy 0 to znaczy ze nie usuwamy i jako trzeci mozemy wstawic nowy obiekt wstawiany za obecny np. {nowy}
		$scope.users.splice( $index , 1 );

		$http.post( 'api/admin/users/delete/' , {
			token: checkToken.raw(),
			user : user
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};


}]);

//routeParams pozwoli na pobranie parametrów które zostały przekazane w parametrze przeglądarki w routerze

controllersAdmin.controller( 'userEdit' , [ '$scope' , '$http' , '$routeParams' , '$timeout' , 'checkToken' , function( $scope , $http , $routeParams , $timeout , checkToken ){


    //console log printuje wartość id pobranego z routera po kliknięciu edycji konkretnego produktu (nazwa atrybutu po routeParams musi byc identyczna z tą określoną w state url w routerze)
    console.log('params ID', $routeParams.id);


	var userId = $routeParams.id;
	$scope.id = userId;

	$http.post( 'api/admin/users/get/' + userId , {
		token: checkToken.raw()
	}).success( function( data ){
		$scope.user = data;
		console.log( data );
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
    $scope.saveChanges = function ( user ) {

		$http.post( 'api/admin/users/update/' , {
			token: checkToken.raw(),
			user : user,
			id : userId,
			name : user.name,
			email : user.email,
			password : user.password,
			passconf : user.passconf
		}).success( function( errors ){

			$scope.submit = true;
			
			if ( errors )
			{
				$scope.errors = errors;
			}
			else
			{
				$scope.success = true;
				$timeout(function(){
					$scope.success = false;
					$scope.product = {};
				} , 3000 );
			}

		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};


}]);


controllersAdmin.controller( 'userCreate' , [ '$scope' , '$http' , '$timeout' , 'checkToken' , function( $scope , $http , $timeout , checkToken ){
    //ustawienie domyślnej roli dla tworzonego użytkownika na user
	$scope.user = {};
	$scope.user.role = 'user';

    //funkcja zapisująca zmiany po wypelnieniu formularza i wcisnięciu buttona "zapisz"
	$scope.createUser = function ( user ) {

		$http.post( 'api/admin/users/create/' , {
            //istotne jest przekazanie nie tylko całego uzytkownika, ale też poszczególnych jego inputów po to by można się było do nich odwołać w walidacji codeigniter
            token: checkToken.raw(),
			user : user,
			name : user.name,
			email : user.email,
			password : user.password,
			passconf : user.passconf
		}).success( function( errors ){

			$scope.submit = true;
			
			if ( errors )
			{
				$scope.errors = errors;
			}
			else
			{
				$scope.success = true;
                //angularowy timeout uzyty tutaj w celu przypisania do powyzszego success wartosci false po 3 sekundach, co spowoduje zniknięcie alertu o sukcesie po trzech sekundach , a także wyczyszcenie inputów ze scope(zeby nie wisial niepotrzebnie na stronie )
                $timeout(function(){
					$scope.success = false;
					$scope.product = {};
				} , 3000 );
			}
			
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};

}]);


controllersAdmin.controller( 'orders' , [ '$scope' , '$http' , 'checkToken' , function( $scope , $http , checkToken ){

	$http.post( 'api/admin/orders/get/' , {

		token: checkToken.raw(),
		payload: checkToken.payload()
        //callback wywołujący się, kiedy połączymy się z bazą
	}).success( function( data ){
        // przypisywanie zmiennej scope do odpowiedzi z pliku
		$scope.orders = data;

		console.log( data );

		angular.forEach( $scope.orders , function( order , key ){
			var parsed = JSON.parse( order.items );
			$scope.orders[key].items = parsed;
		});
        //callback kiedy jest błąd
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

	$scope.delete = function ( order , $index ) {
        //jeżeli admin nie potwierdzi usuwania, to skrypt poniżej zostanie zatrzymany
		if ( !confirm( 'Czy na pewno chcesz usunąć to zdjęcie' ) )
			return false;
        // spliceowanie tablicy param 1 - index usuwanego obiektu; param 2 - ile elementów od tego elementu usunąć; param 3 - jezeli w drugim podamy 0 to znaczy ze nie usuwamy i jako trzeci mozemy wstawic nowy obiekt wstawiany za obecny np. {nowy}
		$scope.orders.splice( $index , 1 );
        console.log('usunięto zamówienie: ', user, 'o indexie: ', $index);

		$http.post( 'api/admin/orders/delete/' , {
			token: checkToken.raw(),
			id: order.id
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};

    //funkcja powoduje zmiane statusu z 0 na 1 , czyli z oczekującego na wysłane po kliku
	$scope.changeStatus = function ( order ) {

		if ( order.status == 0 )
			order.status = 1;
		else
			order.status = 0;

		$http.post( 'api/admin/orders/update/' , {
			token: checkToken.raw(),
			id: order.id,
			status : order.status
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};

}]);