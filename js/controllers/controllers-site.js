'use strict';

var controllersSite = angular.module( 'controllersSite' , [] );


controllersSite.controller( 'siteProducts' , [ '$scope' , '$http' , 'cartSrv' , function( $scope , $http , cartSrv ){
	
	$http.get( 'api/site/products/get' )
    //callback wywołujący się, kiedy połączymy się z bazą
	.success( function( data ){
        // przypisywanie zmiennej scope do odpowiedzi z pliku
		$scope.products = data;
        //callback kiedy jest błąd
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});
    //dodawanie produktu do koszyka po kliknięciu przycisku
	$scope.addToCart = function ( product ) {
		cartSrv.add( product );
	};

    //funckja sprawdzająca stan koszyka w widoku produktów (tak zeby na przycisku dodaj do koszyka, wyswietlala sie aktualna ilosc dodanego produktu)
	$scope.checkCart = function ( product ) {
		if ( cartSrv.show().length )
		{
			angular.forEach( cartSrv.show() , function( item ){
				if ( item.id == product.id )
				{
					product.qty = item.qty;
				}
			});
		}
	}
}]);


controllersSite.controller( 'siteProduct' , [ '$scope' , '$http' , '$routeParams' , 'cartSrv' , function( $scope , $http , $routeParams , cartSrv ){
//routeParams pozwoli na pobranie parametrów które zostały przekazane w parametrze przeglądarki w routerze


	var id = $routeParams.id;

    // wywołanie zapytania http do pliku json
	$http.post( 'api/site/products/get/' + id )
    //callback wywołujący się, kiedy połączymy się z bazą
	.success( function( data ){
        // przypisywanie zmiennej scope do odpowiedzi z pliku , przypisuje wszystko co zostanie odesłane
		$scope.product = data;
		$scope.checkCart( $scope.product );
        //callback kiedy jest błąd
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

    //dodawanie produktu do koszyka po kliknięciu przycisku
	$scope.addToCart = function ( product ) {
		cartSrv.add( product );
	};
    //funckja sprawdzająca stan koszyka w widoku produktów (tak zeby na przycisku dodaj do koszyka, wyswietlala sie aktualna ilosc dodanego produktu)
	$scope.checkCart = function ( product ) {
		if ( cartSrv.show().length )
		{
			angular.forEach( cartSrv.show() , function( item ){
				if ( item.id == product.id )
				{
					product.qty = item.qty;
				}
			});
		}
	};

	function getImages() {
        //zapytanie o zdjęcia obiektu o konkretnym id
		$http.get( 'api/site/products/getImages/' + id ).
		success( function( data ){
			$scope.images = data; 
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});
	}
	getImages();

}]);


controllersSite.controller( 'siteOrders' , [ '$scope' , '$http' , 'checkToken' , function( $scope , $http , checkToken ){
    // wywołanie zapytania http do pliku json
	$http.post( 'api/site/orders/get/' , {

		token: checkToken.raw(),
		payload: checkToken.payload()
		//callback wywołujący się, kiedy połączymy się z bazą
	}).success( function( data ){
        // przypisywanie zmiennej scope do odpowiedzi z pliku
		$scope.orders = data;

		angular.forEach( $scope.orders , function( order , key ){
			var parsed = JSON.parse( order.items );
			$scope.orders[key].items = parsed;
		});
        //callback kiedy jest błąd
	}).error( function(){
		console.log( 'Błąd połączenia z API' );
	});

}]);


controllersSite.controller( 'cartCtrl' , [ '$scope' , '$http' , '$filter' , 'cartSrv' , 'checkToken' , function( $scope , $http , $filter , cartSrv , checkToken ){
    //wchodząc na podstronę koszyka ,wywoływany jest serwis cartSrv z funkcją show ,co powoduje wyświetlenie zawartosci z localstorage
	$scope.cart = cartSrv.show();

	//funkcja czyszcząca koszyk z localstorage
	$scope.emptyCart = function () {
		cartSrv.empty();
	};

    //funckja zwracająca końcową cenę koszyka, iteracja po tabeli cart
    $scope.total = function () {
		var total = 0;
		angular.forEach( $scope.cart , function ( item ) {
			total += item.qty * item.price;
		});
        //zanim zostanie zwrócony total, zaokrąglamy go do 2 m. po przecinku
		total = $filter( 'number' )( total , 2 );
		return total;
	};

	$scope.removeItem = function ( $index ) {
        //usuwanie z widoku
        $scope.cart.splice( $index , 1 );
        //usuwanie z serwisu
        cartSrv.update( $scope.cart );
	};

	$scope.setOrder = function ( $event ) {
        //prevent default zeby formularz zachowywał się tak jak to zaprogramowane, a nie domyslnie
		$event.preventDefault();

        //sprawdz czy użytkownik jest zalogowany używając tokena
        if ( !checkToken.loggedIn() )
		{
			$scope.alert = { type : 'warning' , msg : 'Musisz być zalogowany, żeby złożyć zamówienie.' };
			return false;
		}


		$http.post( 'api/site/orders/create/' , {

			token: checkToken.raw(),
			payload: checkToken.payload(),
			items: $scope.cart,
			total: $scope.total()
        //callback wywołujący się, kiedy połączymy się z bazą
		}).success( function( data ){
            // czyszczenie koszyka po złożeniu zamówienia
			cartSrv.empty();
            //alert wyświetlający sukces zakupów
            $scope.alert = { type : 'success' , msg : 'Zamówienie złożone. Nie odświeżaj strony. Trwa przekierowywanie do płatności...' };
            //uruchomienie formularza Paypal za pomocą jQuery
			$( '#paypalForm' ).submit();
            //callback kiedy jest błąd
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};
    //obserwowanie zmian koszyka po zmianie np. ilosci
	$scope.$watch( function (){
		cartSrv.update( $scope.cart );
	});

}]);


controllersSite.controller( 'login' , [ '$scope' , '$http' , 'store' , 'checkToken' , '$location' , function( $scope , $http , store , checkToken , $location ){
    //jezeli użytkownik jest zalogowany, przenies go do strony produktów - z użyciem state.go z ui-router
	if ( checkToken.loggedIn() )
		$location.path( '/products' );

	$scope.user = {};

	$scope.formSubmit = function ( user ) {

		$http.post( 'api/site/user/login/' , {
            //istotne jest przekazanie inputów po to by można się było do nich odwołać w walidacji codeigniter
            email : user.email,
			password : user.password
            //callback wywołujący się, kiedy połączymy się z bazą
		}).success( function( data ){

			$scope.submit = true;
            //przypisanie scope error z widoku do odpowiedzi z serwera z własciwoscią error określoną w php w funkcji login
			$scope.error = data.error;
            //jezeli nieprawda, że odpowiedz zawiera error , wtedy w localstorage zapisz obiekt o nazwie token zawierający odpowiedz z serwera o nazwie token ( z funkcji login w php)
			if ( !data.error )
			{
				store.set( 'token' , data.token );
				location.reload();
			}
            //callback kiedy jest błąd
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};

}]);


controllersSite.controller( 'register' , [ '$scope' , '$http' , function( $scope , $http ){

	$scope.user = {};

	$scope.formSubmit = function ( user ) {
        //istotne jest przekazanie nie tylko całego uzytkownika, ale też poszczególnych jego inputów po to by można się było do nich odwołać w walidacji codeigniter
		$http.post( 'api/site/user/create/' , {
			user : user,
			name : user.name,
			email : user.email,
			password : user.password,
			passconf : user.passconf
        //callback wywołujący się, kiedy połączymy się z bazą
		}).success( function( errors ){

			$scope.submit = true;
            //wyczyszcenie inputów ze scope(zeby nie wisial niepotrzebnie na stronie )
			$scope.user = {};
			
			if ( errors )
			{
				$scope.errors = errors;
			}
			else
			{
				$scope.errors = {};
				$scope.success = true;
			}
        //callback kiedy jest błąd
        }).error( function(){
			console.log( 'Błąd połączenia z API' );
		});


	};

}]);