'use strict';

var myServices = angular.module( 'myServices' , [] );

//factory zawsze musi cos zwrocic, w tym przypadku return cart

myServices.factory( 'cartSrv' , [ 'store' , function( store ) {

    //get pobiera zawartosc koszyka z localstorage , set nadpisuje do koszyka - dwa arg. 1. nazwa 2. zawartosc, remove usuwa zawartosc
    // store.get('cart');
    // store.set('cart', 'zawartosc koszyka');
    // store.remove('cart');

    //jezeli aplikacja wykryje ze jest juz jakas zawartosc koszuka, to wtedy koszyk = obecna zawartosc, w przeciwnym razie cart koszyk jest pusty
    if ( store.get( 'cart' ) )
		var cart = store.get( 'cart' );
	else
		var cart = [];

	//pokazuje zawartosc obecną koszyka
	cart.show = function () {
		return cart;
	};

    //dodawanie obiektów do koszyka ,zapisywanie w local storage
	cart.add = function ( product ) {

        //jezeli dlugosc tablicy z koszyka jest nieprawdziwa (czyli nie ma nic), wtedy przypisuje do własciwosci qty(ilosc) produktu 0, czyli ze nie ma produktów; następnie dodaje kliknięy produkt do tablicy
        //tyczy się tylko pierwszego obiektu w koszyku
		if ( !cart.length )
		{
			product.qty = 0;
			cart.push( product );
		}

        //jeżeli jednak dany produkt istnieje już w koszyku - wtedy patrz poniżej
        //pętla angularowa forEach param1 - zbiór elementów, param2 - funkcja przyjmująca parametry value (zwraca zawartosc obiektu) i key (klucz - np ID lub index)

        //zmienna add new do dodawania nowego obiektu (a nie pierwszego czy kolejnego)
        var addNew = true;
		angular.forEach( cart , function ( value , key ) {

			// TODO: zmienić name na id gdy będzie kontakt z bazą
            //sprawdzane jest czy zawartosc koszuka zawiera juz takie same produkty w koszyku, jezeli tak, to zamiast dodawac kolejne , zwiekszamy wartosc qty(ilosc) o 1 w zmiennej cart dla konkretnego obiektu; add new ustawiamy na false bo nie dodajemy nowego produktu tylko inkrementujemy ilość tego samego obiektu
			if ( value.name == product.name )
			{
				addNew = false;
				cart[key].qty++;
			}
		});

        //jeżeli jednak jest to nowy obiekt w koszyku, to ustawiamy wartosc qty na 1 i pushujemy go do tablicy koszyka
        if ( addNew )
		{
			product.qty = 1;
			cart.push( product );
		}

		store.set( 'cart' , cart.show() );

	};

    //czyszczenie local storage koszyka
	cart.empty = function () {
		store.remove( 'cart' );
		cart.length = 0;
	};

    //updateowanie koszyka po zmianach zachodzących w koszyku
    cart.update = function ( newCart ) {
		store.set( 'cart' , newCart );
	};

	return cart;
	
}]);


myServices.service( 'checkToken' , [ 'store' , 'jwtHelper' , function( store , jwtHelper ) {

	var token = store.get( 'token' );

	if ( token )
		token = jwtHelper.decodeToken( token );
	else
		token = false;

	this.payload = function () {
		return token;
	};

	this.loggedIn = function () {
		if ( token )
			return true;
		else
			return false;
	};

	this.isAdmin = function () {
		if ( token.role == 'admin' )
			return true;
		else
			return false;
	};

	this.raw = function () {
		return store.get( 'token' );
	};

	this.del = function () {
		store.remove( 'token' );
	};


}]);